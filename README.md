MV Shopline Itau
=======================

Introduction
------------
This project is a module of payment using the ItauShopline service using Zend Framework 2. 

Installation
------------

Using Composer (recommended)
----------------------------
The recommended way is clone the repository and manually invoke `composer` using the shipped
`composer.phar`:

    cd my/project/dir
    git clone git://github.com/marcusvy/mv-shopline-itau.git
    cd mv-shopline-itau
    php composer.phar self-update
    php composer.phar install

(The `self-update` directive is to ensure you have an up-to-date `composer.phar`
available.)

Web Server Setup
----------------

### PHP CLI Server

The simplest way to get started if you are using PHP 5.4 or above is to start the internal PHP cli-server in the root directory:

    php -S 0.0.0.0:8080 -t public/ public/index.php

This will start the cli-server on port 8080, and bind it to all network
interfaces.

**Note: ** The built-in CLI server is *for development only*.

### Apache Setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

    <VirtualHost *:80>
        ServerName mv-shopline-itau.dev
        DocumentRoot /path/to/mv-shopline-itau/public
        SetEnv APPLICATION_ENV "development"
        <Directory /path/to/mv-shopline-itau/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>
