$(document).ready(function () {
  var mvForm = $(document.forms['mvForm']);
  var inputCPF = $('input[name=cpf]');
  var inputCNPJ = $('input[name=cnpj]');

  initCgc();
  showCgc($('select[name=codigoInscricao]')[0].value);


  $('select[name=codigoInscricao]').on('change', function (e) {
    resetCgc();
    showCgc(this.value);
  });

  $('input[name=cpf]').on('keyup', function (e) {
    $('input[name=numeroInscricao]').val(this.value);
    console.log(this.value);
  });

  $('input[name=cnpj]').on('keyup', function (e) {
    $('input[name=numeroInscricao]').val(this.value);
    console.log(this.value);
  });

  /**
   * Inicializa Cgc
   */
  function initCgc(){
    if ($('select[name=codigoInscricao]')[0].value == '01') {
      fieldVisible(inputCNPJ, false);
    }
    if ($('select[name=codigoInscricao]')[0].value == '02') {
      fieldVisible(inputCPF, false);
    }
  }

  /**
   * Reseta cgc
   */
  function resetCgc(){
    $('input[name=cpf]').val('');
    $('input[name=cnpj]').val('');
    fieldVisible(inputCPF, false);
    fieldVisible(inputCNPJ, false);
  }
  /**
   *
   * @param {jQuery} field
   * @param {boolean} status
   */
  function fieldVisible(field, status) {
    var status = (status) ? 'block' : 'none';
    field.parent().parent().css('display', status);
  }


  /**
   * Mostra cgc correto
   * @param value
   */
  function showCgc(value) {
    if (value != '') {
      switch (value) {
        case '01':
          fieldVisible(inputCPF, true);
          break;
        case '02':
          fieldVisible(inputCNPJ, true);
          break;
      }
    }
  }
});

function carregabrw() {
  window.open('', 'shopline', 'toolbar=yes,menubar=yes,resizable=yes,status=no,scrollbars=yes, width = 815, height = 575');
}
