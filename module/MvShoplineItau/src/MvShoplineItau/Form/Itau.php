<?php
namespace MvShoplineItau\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

class Itau
  extends Form
  implements InputFilterProviderInterface
{

  public function init()
  {
    $this->add(array(
      'name' => 'DC',
      'type' => 'Hidden'
    ));
  }

  public function getInputFilterSpecification()
  {

    return array(
      'DC' => array(
        'required' => true,
      ),
    );
  }

}
