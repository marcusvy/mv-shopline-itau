<?php
namespace MvShoplineItau\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

class Sacado
  extends Form
  implements InputFilterProviderInterface
{

  public function init()
  {
    $estadosBrasileiros = array(
      "AC" => "Acre",
      "AL" => "Alagoas",
      "AM" => "Amazonas",
      "AP" => "Amapá",
      "BA" => "Bahia",
      "CE" => "Ceará",
      "DF" => "Distrito Federal",
      "ES" => "Espírito Santo",
      "GO" => "Goiás",
      "MA" => "Maranhão",
      "MT" => "Mato Grosso",
      "MS" => "Mato Grosso do Sul",
      "MG" => "Minas Gerais",
      "PA" => "Pará",
      "PB" => "Paraíba",
      "PR" => "Paraná",
      "PE" => "Pernambuco",
      "PI" => "Piauí",
      "RJ" => "Rio de Janeiro",
      "RN" => "Rio Grande do Norte",
      "RO" => "Rondônia",
      "RS" => "Rio Grande do Sul",
      "RR" => "Roraima",
      "SC" => "Santa Catarina",
      "SE" => "Sergipe",
      "SP" => "São Paulo",
      "TO" => "Tocantins"
    );

//    $nomeSacado = 'Marcus Vinicius da Rocha Gouve';
    $codigoInscricao = '01';
    $numeroInscricao = '87668718249';// cpf ou cnpj
    $enderecoSacado = 'Rua Capitão Natanael Aguiar, 1645'; //endereço da pessoa
    $bairroSacado = 'Agenor de Carvalho'; //bairro da pessoa
    $cepSacado = '76820270'; //cep da pessoa
    $cidadeSacado = 'Porto Velho'; //cidade da pessoa
    $estadoSacado = 'RO'; // 2digitos da pessoa

    //nome completo
    $this->add(array(
      'name' => 'nomeSacado',
      'type' => 'Text',
      'options' => array(
        'label' => 'Nome Completo'
      ),
      'attributes' => array(
        'placeholder' => 'Digite seu nome completo',
        'maxlength' => 30
      )
    ));

    //cpf: 01 ou cnpj: 02
    $this->add(array(
      'name' => 'codigoInscricao',
      'type' => 'Select',
      'options' => array(
        'label' => 'Tipo e inscrição',
        'disable_inarray_validator' => true,
        'empty_option' => 'Selecione...',
        'value_options' => array(
          '01' => 'CPF',
          '02' => 'CNPJ',
        )
      )
    ));
    $this->add(array(
      'name' => 'numeroInscricao',
      'type' => 'Hidden'
    ));

    $this->add(array(
      'name' => 'cpf',
      'type' => 'Text',
      'options' => array(
        'label' => 'CPF'
      ),
      'attributes' => array(
        'placeholder' => 'Somente números',
        'maxlength' => 11
      )
    ));

    $this->add(array(
      'name' => 'cnpj',
      'type' => 'Text',
      'options' => array(
        'label' => 'CNPJ'
      ),
      'attributes' => array(
        'placeholder' => 'Somente números',
        'maxlength' => 14
      )
    ));

    $this->add(array(
      'name' => 'cepSacado',
      'type' => 'Text',
      'options' => array(
        'label' => 'CEP'
      ),
      'attributes' => array(
        'placeholder' => 'Somente números',
        'maxlength' => 8
      )
    ));

    $this->add(array(
      'name' => 'enderecoSacado',
      'type' => 'Text',
      'options' => array(
        'label' => 'Endereço'
      ),
      'attributes' => array(
        'placeholder' => 'Minha rua, Número',
        'maxlength' => 40
      )
    ));
    $this->add(array(
      'name' => 'bairroSacado',
      'type' => 'Text',
      'options' => array(
        'label' => 'Bairro'
      ),
      'attributes' => array(
        'maxlength' => 15
      )
    ));

    $this->add(array(
      'name' => 'cidadeSacado',
      'type' => 'Text',
      'options' => array(
        'label' => 'Cidade',
      ),
      'attributes' => array(
        'maxlength' => 15
      )
    ));
    $this->add(array(
      'name' => 'estadoSacado',
      'type' => 'Select',
      'options' => array(
        'label' => 'Estado',
        'disable_inarray_validator' => true,
        'empty_option' => 'Selecione...',
        'value_options' => $estadosBrasileiros
      ),
    ));

    $this->add(array(
      'name' => 'valor',
      'type' => 'Text',
      'options' => array(
        'label' => 'Valor'
      )
    ));
  }

  public function getInputFilterSpecification()
  {

    return array(
      'nomeSacado' => array(
        'required' => true,
        'validators' => array(
          ['name' => 'NotEmpty', 'options' => [
            'messages' => array(
              'isEmpty' => 'Você precisa digitar seu nome.'
            )
          ]],
          ['name' => 'StringLength', 'options' => [
            'min' => 3,
            'max' => 30
          ]],
        )
      ),

      'cpf' => array(
        'required' => false,
        'validators' => array(
          ['name' => 'NotEmpty', 'options' => [
            'messages' => array(
              'isEmpty' => 'Você precisa digitar seu CPF'
            )
          ]],
          ['name' => 'MvShoplineItau\Validator\Cpf'],
        )
      ),

      'cnpj' => array(
        'required' => false,
        'validators' => array(
          ['name' => 'MvShoplineItau\Validator\Cnpj']
        )
      ),

      'codigoInscricao' => array(
        'required' => true,
        'validators' => array(
          ['name' => 'NotEmpty', 'options' => [
            'messages' => array(
              'isEmpty' => 'Você precisa digitar seu CPF ou CNPJ.'
            )
          ]],
          ['name' => 'StringLength', 'options' => [
            'min' => 2,
            'max' => 2
          ]],
        )
      ),

      'numeroInscricao' => array(
        'required' => true,
        'validators' => array(
          ['name' => 'NotEmpty', 'options' => [
            'messages' => array(
              'isEmpty' => 'Você precisa digitar seu CPF ou CNPJ.'
            )
          ]],
          ['name' => 'StringLength', 'options' => [
            'max' => 14
          ]],
        )
      ),
      'enderecoSacado' => array(
        'required' => true,
        'validators' => array(
          ['name' => 'NotEmpty', 'options' => [
            'messages' => array(
              'isEmpty' => 'Você precisa digitar seu Endereço.'
            )
          ]],
          ['name' => 'StringLength', 'options' => [
            'min' => 3,
            'max' => 40
          ]],
        )
      ),
      'bairroSacado' => array(
        'required' => true,
        'validators' => array(
          ['name' => 'NotEmpty', 'options' => [
            'messages' => array(
              'isEmpty' => 'Você precisa digitar um Bairro.'
            )
          ]],
          ['name' => 'StringLength', 'options' => [
            'max' => 15
          ]],
        )
      ),
      'cepSacado' => array(
        'required' => true,
        'validators' => array(
          ['name' => 'NotEmpty', 'options' => [
            'messages' => array(
              'isEmpty' => 'Você precisa digitar um CEP válido.'
            )
          ]],
          ['name' => 'StringLength', 'options' => [
            'min' => 8,
            'max' => 8
          ]],
        )
      ),
      'cidadeSacado' => array(
        'required' => true,
        'validators' => array(
          ['name' => 'NotEmpty', 'options' => [
            'messages' => array(
              'isEmpty' => 'Você precisa digitar sua Cidade.'
            )
          ]],
          ['name' => 'StringLength', 'options' => [
            'max' => 15
          ]],
        )
      ),
      'estadoSacado' => array(
        'required' => true,
        'validators' => array(
          ['name' => 'NotEmpty', 'options' => [
            'messages' => array(
              'isEmpty' => 'Você precisa escolher seu Estado.'
            )
          ]],
          ['name' => 'StringLength', 'options' => [
            'min' => 2,
            'max' => 2,
          ]],
        )
      ),
      'valor' => array(
        'required' => true,
        'validators' => array(
          ['name' => 'NotEmpty', 'options' => [
            'messages' => array(
              'isEmpty' => 'Você precisa insterir um Valor para Doação'
            )
          ]],
        )
      ),
    );
  }

}
