<?php

namespace MvShoplineItau\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use MvShoplineItau\Itau\Itaucripto;

class IndexController extends AbstractActionController
{

  public function indexAction()
  {
    /** @var \MvShoplineItau\Form\Sacado $form */
    $form = $this->getServiceLocator()
      ->get('FormElementManager')
      ->get('MvShoplineItau\Form\Sacado');

    /** @var \MvShoplineItau\Form\Itau $formItau */
    $formItau = $this->getServiceLocator()
      ->get('FormElementManager')
      ->get('MvShoplineItau\Form\Itau');

    $viewGenerate = false;
    $request = $this->getRequest();


    if ($request->isPost()) {
      /** @var \Zend\Stdlib\Parameters $data */
      $data = $request->getPost();

      $codInscricao = $data->get('codigoInscricao');

      if ($codInscricao == '01' && !empty($data->get('cpf'))) {
        $data->set('numeroInscricao', $data->get('cpf'));
      }
      if ($codInscricao == '01' && !empty($data->get('cnpj'))) {
        $data->set('numeroInscricao', $data->get('cnpj'));
      }

      $form->setData($data);

      if ($form->isValid()) {

        $cripto = new Itaucripto();
        $formData = $form->getData();
        $config = $this->getServiceLocator()->get('Config');
        $dadosCedente = $config['itaushopline'];
        $viewGenerate = true;


        // Dados empresa
        $codEmp = $dadosCedente['codEmp'];
        $chave = $dadosCedente['key'];
        $pedido = (new \DateTime())->format('dm') . rand(102, 9999);

        // Dados sacado
        $valor = $formData['valor'];
        $nomeSacado = $formData['nomeSacado'];
        $codigoInscricao = $formData['codigoInscricao'];
        $numeroInscricao = $formData['numeroInscricao'];
        $enderecoSacado = $formData['enderecoSacado'];
        $bairroSacado = $formData['bairroSacado'];
        $cepSacado = $formData['cepSacado'];
        $cidadeSacado = $formData['cidadeSacado'];
        $estadoSacado = $formData['estadoSacado'];
        $dataVencimento = (new \DateTime('now'))->add(new \DateInterval('P10D'))->format('dmY');

        // Opicionais
        $urlRetorna = '';
        $observacao = '';
        $obsAd1 = '';
        $obsAd2 = '';
        $obsAd3 = '';

        $dataItau = array('DC' => $cripto->geraDados($codEmp,
          $pedido,
          $valor,
          $observacao,
          $chave,
          $nomeSacado,
          $codigoInscricao,
          $numeroInscricao,
          $enderecoSacado,
          $bairroSacado,
          $cepSacado,
          $cidadeSacado,
          $estadoSacado,
          $dataVencimento,
          $urlRetorna,
          $obsAd1,
          $obsAd2,
          $obsAd3)
        );
        $formItau->setData($dataItau);
      }

    }
    return new ViewModel(array(
      'form' => $form,
      'formItau' => $formItau,
      'viewGenerate' => $viewGenerate,
      'dadoSacado' => $form->getData()
    ));
  }


}

