<?php
namespace MvShoplineItau;

return array(
  'controllers' => array(
    'invokables' => [
      'MvShoplineItau\Controller\Index' => 'MvShoplineItau\Controller\IndexController',
    ]
  ),
);
