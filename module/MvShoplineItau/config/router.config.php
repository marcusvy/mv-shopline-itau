<?php
namespace MvShoplineItau;

return array(
  'router' => array(
    'routes' => array(
      'mv-app-shopline-itau' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route'    => '/mv-app/shopline-itau',
          'defaults' => array(
            'controller' => 'MvShoplineItau\Controller\Index',
            'action'     => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'default' => array(
            'type'    => 'Segment',
            'options' => array(
              'route'    => '[:controller][/:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(
                '__NAMESPACE__' => 'MvShoplineItau\Controller',
                'controller'=>'Index'
              ),
            ),
          ),
        ),
      ),

    )
  )
);
