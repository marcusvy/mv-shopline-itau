<?php
namespace MvShoplineItau;

return array(
  'view_manager' => array(
    'template_map' => array(
      'mv-shopline-itau/index/index' => __DIR__ . '/../view/mv-shopline-itau/index/index.phtml',
    ),
    'template_path_stack' => array(
      __DIR__ . '/../view',
    ),
  ),
);
