<?php
namespace MvShoplineItau;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Validator\AbstractValidator;
use Zend\Mvc\I18n\Translator;

class Module
{
  public function getConfig()
  {
    return array_merge(
      include __DIR__ . '/config/controllers.config.php',
      include __DIR__ . '/config/router.config.php',
      include __DIR__ . '/config/service_manager.config.php',
      include __DIR__ . '/config/view_manager.config.php'
    );
  }

  public function getAutoloaderConfig()
  {
    return array(
      'Zend\Loader\StandardAutoloader' => array(
        'namespaces' => array(
          __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
        ),
      ),
    );
  }

  public function onBootstrap(MvcEvent $e)
  {
    $eventManager = $e->getApplication()->getEventManager();
    $moduleRouteListener = new ModuleRouteListener();
    $moduleRouteListener->attach($eventManager);

    //Esse é o código para a tradução

    //Pega o serviço translator definido no arquivo module.config.php (aliases)
    $translator = $e->getApplication()
      ->getServiceManager()
      ->get('translator');

    //Define o local onde se encontra o arquivo de tradução de mensagens
    $translator->addTranslationFile('phpArray', './vendor/zendframework/zendframework/resources/languages/pt_BR/Zend_Validate.php');

    //Define o local (você também pode definir diretamente no método acima
    $translator->setLocale('pt_BR');
    //Define a tradução padrão do Validator
    AbstractValidator::setDefaultTranslator(new Translator($translator));
  }
}
